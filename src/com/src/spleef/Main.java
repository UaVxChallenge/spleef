package com.src.spleef;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.src.spleef.arenas.ArenaManager;
import com.src.spleef.listeners.Block;
import com.src.spleef.listeners.CanMove;
import com.src.spleef.listeners.PlayerLeave;
import com.src.spleef.listeners.Signs;

public class Main extends JavaPlugin{
	
	public static Plugin instance;

	@SuppressWarnings("deprecation")
	@Override
	public void onEnable(){
		instance = this;
		
        if(!getDataFolder().exists())
            getDataFolder().mkdir();

        if(getConfig() == null)
            saveDefaultConfig();

        ArenaManager.getManager().loadGames();
		
		Bukkit.getServer().getPluginManager().registerEvents(new Block(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Signs(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new CanMove(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerLeave(), this);

		
		initializeConfig();
		
		for(Player p : Bukkit.getOnlinePlayers()){
			p.canSee(p);
		}
	}
	
	@Override
	public void onDisable(){
		 saveConfig();
	}
	
	public void initializeConfig(){		
		getConfig().set("Default", " ");
		this.saveConfig();
	}

	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args){
		
		if(!(sender instanceof Player)){
			return true;
		}
		
		final Player p = (Player) sender;
		
		if(cmd.getLabel().equalsIgnoreCase("Spleef")){
			
			if(args.length < 1){
				return false;
			}
			
			if(args[0].equalsIgnoreCase("Spawn1")){
				getConfig().set("Spawn1.World", p.getWorld().getName().toString());
				getConfig().set("Spawn1.X", p.getLocation().getBlockX());
				getConfig().set("Spawn1.Y", p.getLocation().getBlockY());
				getConfig().set("Spawn1.Z", p.getLocation().getBlockZ());
				p.sendMessage(ChatColor.GOLD + "Spawn 1 has been set!");
				
				saveConfig();
				return true;
			}
			
			if(args[0].equalsIgnoreCase("Spawn2")){
				getConfig().set("Spawn2.World", p.getWorld().getName().toString());
				getConfig().set("Spawn2.X", p.getLocation().getBlockX());
				getConfig().set("Spawn2.Y", p.getLocation().getBlockY());
				getConfig().set("Spawn2.Z", p.getLocation().getBlockZ());
				p.sendMessage(ChatColor.GOLD + "Spawn 2 has been set!");
				saveConfig();
				return true;
			}
			if(args[0].equalsIgnoreCase("Create")){
				
				ArenaManager.getManager().createArena();
				
				return true;
			}
			if(args[0].equalsIgnoreCase("Leave")){
	        	
				ArenaManager.getManager().removePlayer(p);
				
				return true;
			}
		}
		
		return false;
	}
	
	
	
}
