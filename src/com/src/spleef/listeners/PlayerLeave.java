package com.src.spleef.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.src.spleef.arenas.Arena;
import com.src.spleef.arenas.ArenaManager;
import com.src.spleef.utils.LocationUtils;

public class PlayerLeave implements Listener{

	@SuppressWarnings("deprecation")
	@EventHandler
	public void playerLeave(PlayerQuitEvent e){
		
		
	       Arena a = null;
	        for(Arena arena : ArenaManager.arenas){
	            if(arena.getPlayers().contains(e.getPlayer().getName())){
	                a = arena;
	            }
	        }
	        
	        if(a == null) return;

	        	for(Player pl : Bukkit.getOnlinePlayers()){
	        		
	        		if(a.getPlayers().contains(pl.getName())){
	        			
	        		
	    			for(int x = LocationUtils.Floor1.getBlockX(); x < LocationUtils.Floor2.getBlockX(); x++){
	    				for(int z = LocationUtils.Floor1.getBlockZ(); z < LocationUtils.Floor2.getBlockZ(); z++){
	    					
	    					pl.sendBlockChange(new Location(LocationUtils.Floor1.getWorld(), x, LocationUtils.Floor1.getBlockY(), z), Material.SNOW_BLOCK, (byte) 1);
	    					
	    					
	    				}
	    			}
	    			for(int x = LocationUtils.Floor2.getBlockX(); x < LocationUtils.Floor1.getBlockX(); x++){
	    				for(int z = LocationUtils.Floor2.getBlockZ(); z < LocationUtils.Floor1.getBlockZ(); z++){
	    					
	    					pl.sendBlockChange(new Location(LocationUtils.Floor1.getWorld(), x, LocationUtils.Floor1.getBlockY(), z), Material.SNOW_BLOCK, (byte) 1);
	    					
	    					
	    				}
	    			}
	    			if(pl.getName() != e.getPlayer().getName()){
	    	        	ArenaManager.getManager().Winner(pl);
	    			}
	        		ArenaManager.getManager().removePlayer(pl);
	        		}
	        		
	        	}
	        	
	        	a.getPlayers().remove(e.getPlayer().getName());
	        
		
	}
	
	
}
