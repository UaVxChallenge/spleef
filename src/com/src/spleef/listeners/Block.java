package com.src.spleef.listeners;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.src.spleef.Main;
import com.src.spleef.arenas.Arena;
import com.src.spleef.arenas.ArenaManager;
import com.src.spleef.utils.LocationUtils;

public class Block implements Listener{
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void setFloor(PlayerInteractEvent e){
		
		if(e.getPlayer().hasPermission("Spleef.Admin") && e.getPlayer().getGameMode().equals(GameMode.ADVENTURE)){
			
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
				 Main.instance.getConfig().set("WorldFloor.1.World", e.getClickedBlock().getWorld().getName().toString());
				 Main.instance.getConfig().set("WorldFloor.1.X", e.getClickedBlock().getLocation().getBlockX());
				 Main.instance.getConfig().set("WorldFloor.1.Y", e.getClickedBlock().getLocation().getBlockY());
				 Main.instance.getConfig().set("WorldFloor.1.Z", e.getClickedBlock().getLocation().getBlockZ());
				 Main.instance.saveConfig();
			}
			if(e.getAction() == Action.LEFT_CLICK_BLOCK){
				
				e.setCancelled(true);

				 Main.instance.getConfig().set("WorldFloor.2.World", e.getClickedBlock().getWorld().getName().toString());
				 Main.instance.getConfig().set("WorldFloor.2.X", e.getClickedBlock().getLocation().getBlockX());
				 Main.instance.getConfig().set("WorldFloor.2.Y", e.getClickedBlock().getLocation().getBlockY());
				 Main.instance.getConfig().set("WorldFloor.2.Z", e.getClickedBlock().getLocation().getBlockZ());
				 Main.instance.saveConfig();
			}
						
		}
		
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		
	       Arena a = null;
	        for(Arena arena : ArenaManager.arenas){
	            if(arena.getPlayers().contains(e.getPlayer().getName())){
	                a = arena;
	            }
	        }
	        
	        if(a == null){
	        	return;
	        }
	        
	        if(a.getPlayers().contains(e.getPlayer().getName())){
	        	
	        	if(e.getPlayer().getLocation().getBlockY() < LocationUtils.Floor1.getBlockY() - 1){
	        		a.setRound(a.getRound() + 1);
	        		        			        		
	        		for(Player pl : Bukkit.getOnlinePlayers()){
	        			
	        			if(a.getPlayers().contains(pl.getName())){
	        				
	        				
	        				if(!(pl.getName().equalsIgnoreCase(e.getPlayer().getName()))){
	        					if((a.Score.get(pl.getName()) != null)){
        								a.setScore(pl.getName(), a.getScore(pl.getName()) + 1);
        						}	
	        				}

           						if(Arena.Team1.contains(pl.getName())){
           							pl.teleport(LocationUtils.Spawn1);
           						}else if(Arena.Team2.contains(pl.getName())){
           							pl.teleport(LocationUtils.Spawn2);
           					}
           					
	        			
    	        				a.ScoreboardUpdate(pl);
	        		}

		        		if(a.getPlayers().contains(pl.getName())){
	        			for(int x = LocationUtils.Floor1.getBlockX(); x < LocationUtils.Floor2.getBlockX(); x++){
		    				for(int z = LocationUtils.Floor1.getBlockZ(); z < LocationUtils.Floor2.getBlockZ(); z++){
		    					
		    					pl.sendBlockChange(new Location(LocationUtils.Floor1.getWorld(), x, LocationUtils.Floor1.getBlockY(), z), Material.SNOW_BLOCK, (byte) 1);
		    					
		    					
		    				}
		    			}
		    			for(int x = LocationUtils.Floor2.getBlockX(); x < LocationUtils.Floor1.getBlockX(); x++){
		    				for(int z = LocationUtils.Floor2.getBlockZ(); z < LocationUtils.Floor1.getBlockZ(); z++){
		    					
		    					pl.sendBlockChange(new Location(LocationUtils.Floor1.getWorld(), x, LocationUtils.Floor1.getBlockY(), z), Material.SNOW_BLOCK, (byte) 1);
		    					
		    					}
		    				}
		    			}
	        		}
	        	}
	        	if(a.getRound() == 10){

					a.setInGame(false);
					
	        		for(Player p : Bukkit.getOnlinePlayers()){
	        			
	        			if(a.getPlayers().contains(p.getName())){
	        				for(String str : a.getPlayers()){
	        					if(a.getScore(p.getName()) > a.getScore(str) && str != p.getName()){
	        						ArenaManager.getManager().Winner(p);
	        					}
	        					a.Score.remove(p);
		        				a.setRound(1);
		        				
	        				}
	        				ArenaManager.getManager().removePlayer(p);
	        			}
	        			
	        		}
	        		
	        		
	        }
	        }
	        
		
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void playerBlock(PlayerInteractEvent e){
		
	       Arena a = null;
	        for(Arena arena : ArenaManager.arenas){
	            if(arena.getPlayers().contains(e.getPlayer().getName())){
	                a = arena;
	            }
	        }
	        
	        if(a == null){
	        	return;
	        }
	             
	        if(e.getAction() != Action.LEFT_CLICK_BLOCK){
	        	return;
	        }
	        
	        if(a.getPlayers().contains(e.getPlayer().getName())){


				for(Player p : Bukkit.getOnlinePlayers()){
					if(a.getPlayers().contains(p.getName())){
						p.sendBlockChange(e.getClickedBlock().getLocation(), Material.AIR, (byte) 1);
					}else if(!a.getPlayers().contains(p.getName())){
						p.sendBlockChange(e.getClickedBlock().getLocation(), Material.SNOW_BLOCK, (byte) 1);
					}
										
				}
	        }
		
	}

	@EventHandler
	public void onPlayerFall(EntityDamageEvent e){
		
		if(e.getCause() == DamageCause.FALL){
			e.setCancelled(true);
		}
		
	}
	
}
