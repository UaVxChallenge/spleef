package com.src.spleef.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.src.spleef.arenas.ArenaManager;

public class Signs implements Listener{

	@EventHandler
	public void onSignChangeEvent(SignChangeEvent e){
		if(e.getPlayer().hasPermission("Spleef.ADMIN")){
			
			
			if(e.getLine(0).equalsIgnoreCase("[Spleef]") && e.getLine(1).equalsIgnoreCase("join")){
				
				e.setLine(0, ChatColor.BLUE + "[Spleef]");
				e.setLine(1, ChatColor.GREEN + "[join]");

			}
			try{
				Integer.parseInt(e.getLine(2));
			}catch(Exception ex){
				e.getBlock().breakNaturally();
			}
			
			
			
		}
	}
	
	@EventHandler
	public void onPlayerInteractA(PlayerInteractEvent e){
		
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK){
			return;
		}
			if(e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN){

			Sign s = (Sign) e.getClickedBlock().getState();
			
			if(s.getLine(0).equalsIgnoreCase(ChatColor.BLUE + "[Spleef]") && s.getLine(1).equalsIgnoreCase(ChatColor.GREEN + "[join]")){
				
				int id = 0;
				try{
				id = Integer.parseInt(s.getLine(2));
				ArenaManager.getManager().addPlayer(e.getPlayer(), id);

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
			
		}
		
		
	}
	
}
