package com.src.spleef.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.src.spleef.Main;

public class LocationUtils {
	public static Location Spawn1 = new Location(Bukkit.getWorld(Main.instance.getConfig().getString("Spawn1.World")), Main.instance.getConfig().getDouble("Spawn1.X"), Main.instance.getConfig().getDouble("Spawn1.Y"), Main.instance.getConfig().getDouble("Spawn1.Z"));
	public static Location Spawn2 = new Location(Bukkit.getWorld(Main.instance.getConfig().getString("Spawn2.World")), Main.instance.getConfig().getDouble("Spawn2.X"), Main.instance.getConfig().getDouble("Spawn2.Y"), Main.instance.getConfig().getDouble("Spawn2.Z"));
	public static Location Floor2 = new Location(Bukkit.getWorld(Main.instance.getConfig().getString("WorldFloor.2.World")), Main.instance.getConfig().getDouble("WorldFloor.2.X"), Main.instance.getConfig().getDouble("WorldFloor.2.Y"), Main.instance.getConfig().getDouble("WorldFloor.2.Z"));
	public static Location Floor1 = new Location(Bukkit.getWorld(Main.instance.getConfig().getString("WorldFloor.1.World")), Main.instance.getConfig().getDouble("WorldFloor.1.X"), Main.instance.getConfig().getDouble("WorldFloor.1.Y"), Main.instance.getConfig().getDouble("WorldFloor.1.Z"));

}
