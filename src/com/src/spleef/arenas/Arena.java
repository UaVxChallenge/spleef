package com.src.spleef.arenas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class Arena {

	   public int id = 0;
	   public int round = 1;
	    List<String> players = new ArrayList<String>();
	    public static HashMap<Integer, Player> ArenaGame = new HashMap<Integer, Player>();
	    public static ArrayList<String> Team1 = new ArrayList<String>();
	    public static ArrayList<String> Team2 = new ArrayList<String>();
	    public HashMap<String, Integer> Score = new HashMap<String, Integer>();
		public Score Player2;
		public Score Player1;
		public boolean canStart = false;
		public boolean inGame = false;
	    
	    
	    public Arena(int id){
	        this.id = id;
	    }

	    public int getId(){
	        return this.id;
	    }

	    public List<String> getPlayers(){
	        return this.players;
	    }
	
	    public static void addPlayers(int i, Player p){
	    	
	    	ArenaGame.put(i, p);
	    	
	    }
	    
	    public int setRound(int i){
	    	
	    	round = i;
	    	
	    	return round;
	    }
	    
	    public int getRound(){
	    	
	    	return round;
	    }
	    
	    public int getScore(String str){
	    	
	    	return Score.get(str);
	    	
	    }
	    
	    public void setScore(String str,int i){
	    	
	    	Score.put(str, i);
	    	
	    }
	    
	    public static void removePlayer(Player p){
	    	
	    	if(ArenaGame.values().contains(p)){
	    		ArenaGame.values().remove(p);
	    	}
    	
	    }
	    
		@SuppressWarnings("deprecation")
		public void ScoreboardUpdate(Player p){
		        
				Scoreboard game = Bukkit.getScoreboardManager().getNewScoreboard();
				Objective Game = game.registerNewObjective("DuringGame", "DuringGame");
				
				Game.setDisplaySlot(DisplaySlot.SIDEBAR);
				Game.setDisplayName(ChatColor.RED + " " + ChatColor.BOLD + "Score");
				
				Player1 = Game.getScore(ChatColor.AQUA + p.getName() + " ");
				
		        for(Player pl : Bukkit.getOnlinePlayers()){
		        	
			        if(Score.get(pl.getName()) == null){
			        	Score.put(pl.getName(), 0);
			        }
			        
		        	if(getPlayers().contains(pl.getName())){
			
		
			if(!(pl.getName().equalsIgnoreCase(p.getName()))){
				Player2 = Game.getScore(ChatColor.AQUA + pl.getName() + " ");
		        Player2.setScore(this.getScore(pl.getName()));
			}
			
		        	}
		        }
		        
		        Player1.setScore(getScore(p.getName()));
		        
		        for(Player pl : Bukkit.getOnlinePlayers()){
		        	
		        	if(getPlayers().contains(pl.getName())){
		        		pl.setScoreboard(game);
		        	}
		        	
		        }
		        
		}
		
		@SuppressWarnings("deprecation")
		public void removeScoreboard(){
			
			Scoreboard game = Bukkit.getScoreboardManager().getNewScoreboard();
			
			for(Player pl : Bukkit.getOnlinePlayers()){
				if(getPlayers().contains(pl.getName())){
					pl.setScoreboard(game);
				}
			}
		}
	    
	    @SuppressWarnings("deprecation")
		public void doInvis(Player p){
	    	
	    	for(Player pl : Bukkit.getOnlinePlayers()){
	    		
	    		if(!(getPlayers().contains(pl.getName()))){
	    			p.hidePlayer(pl);
	    			
	    		}
	    		
	    	}
	    	
	    }
	    
	    @SuppressWarnings("deprecation")
		public void canSee(Player p){
	    	
	    	for(Player pl : Bukkit.getOnlinePlayers()){
	    		
	    		if(getPlayers().contains(pl.getName())){
	    			pl.showPlayer(p);
	    			p.showPlayer(pl);
	    		}
	    		
	    	}
	    	
	    }
	    
	    public void removeFromTeam(Player p){
	    	
	    	if(Team1.contains(p.getName())){
	    		Team1.remove(p.getName());
	    	}
	    	if(Team2.contains(p.getName())){
	    		Team2.remove(p.getName());
	    		
	    	}
	    	
	    }

	    public void setInGame(boolean b){
	    	
	    	inGame = b;
	    	
	    }
	    
	    public boolean isInGame(){
	    	return inGame;
	    }
	    
}
