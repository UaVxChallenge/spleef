package com.src.spleef.arenas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.src.spleef.Main;
import com.src.spleef.utils.LocationUtils;

public class ArenaManager {
    public Map<String, Location> locs = new HashMap<String, Location>();
    public static ArenaManager am = new ArenaManager();
    Map<String, ItemStack[]> inv = new HashMap<String, ItemStack[]>();
    Map<String, ItemStack[]> armor = new HashMap<String, ItemStack[]>();
    public static List<Arena> arenas = new ArrayList<Arena>();
    int arenaSize = 0;


    public ArenaManager(){

    }

    public static ArenaManager getManager(){
        return am;
    }

    public Arena getArena(int i){
        for(Arena a : arenas){
            if(a.getId() == i){
                return a;
            }
        }
        return null;
    }

    
    @SuppressWarnings("deprecation")
	public void addPlayer(final Player p, int i){
    	Arena a = getArena(i);
        if(a == null){
            p.sendMessage(ChatColor.RED + "Invalid arena!");
            return;
        }     
        if(a.getPlayers().contains(p.getName())){
        	return;
        }
        a.getPlayers().add(p.getName());
        if(a.getPlayers().size() == 1){
        	Arena.Team1.add(p.getName());
        }else if(a.getPlayers().size() == 2){
        	Arena.Team2.add(p.getName());
        }else if(a.getPlayers().size() > 2){
        	p.sendMessage(ChatColor.RED + "Game is full!");
        	return;
        }
        
        inv.put(p.getName(), p.getInventory().getContents());
        armor.put(p.getName(), p.getInventory().getArmorContents());

        p.getInventory().setArmorContents(null);
        p.getInventory().clear();
        p.updateInventory();
        
        ItemStack shovel = new ItemStack(Material.DIAMOND_SPADE);
        
        shovel.addEnchantment(Enchantment.DURABILITY, 3);
        
        p.getInventory().addItem(shovel);
        
        p.updateInventory();
        
        locs.put(p.getName(), p.getLocation());
        Arena.addPlayers(i, p);
        for(Player pl : Bukkit.getOnlinePlayers()){
        	if(a.getPlayers().contains(pl.getName())){
		for(int x = LocationUtils.Floor1.getBlockX(); x < LocationUtils.Floor2.getBlockX(); x++){
			for(int z = LocationUtils.Floor1.getBlockZ(); z < LocationUtils.Floor2.getBlockZ(); z++){
				
				pl.sendBlockChange(new Location(LocationUtils.Floor1.getWorld(), x, LocationUtils.Floor1.getBlockY(), z), Material.SNOW_BLOCK, (byte) 1);
				
				
			}
		}
		for(int x = LocationUtils.Floor2.getBlockX(); x < LocationUtils.Floor1.getBlockX(); x++){
			for(int z = LocationUtils.Floor2.getBlockZ(); z < LocationUtils.Floor1.getBlockZ(); z++){
				
				pl.sendBlockChange(new Location(LocationUtils.Floor1.getWorld(), x, LocationUtils.Floor1.getBlockY(), z), Material.SNOW_BLOCK, (byte) 1);
				
				
			}
		}
        	}
        }
        
        if(Arena.Team1.contains(p.getName())){
        	p.teleport(LocationUtils.Spawn1);
        }
        if(Arena.Team2.contains(p.getName())){
        	p.teleport(LocationUtils.Spawn2);
        }
                
        a.doInvis(p);
        a.canSee(p);
        a.setInGame(true);
        
        p.setGameMode(GameMode.SURVIVAL);
        
        a.ScoreboardUpdate(p);
    }

    
    @SuppressWarnings("deprecation")
	public void removePlayer(Player p){
        Arena a = null;
        for(Arena arena : arenas){
            if(arena.getPlayers().contains(p.getName())){
                a = arena;
            }
        
        }
        
        if(a == null || !a.getPlayers().contains(p.getName())){
            p.sendMessage("Invalid operation!");
            return;
        }
        
        p.getInventory().clear();
        p.updateInventory();

        a.setRound(1);
        
    	for(Player pl : Bukkit.getOnlinePlayers()){
    		
    		if(a.getPlayers().contains(pl.getName())){
    		
    			for(int x = LocationUtils.Floor1.getBlockX(); x < LocationUtils.Floor2.getBlockX(); x++){
    				for(int z = LocationUtils.Floor1.getBlockZ(); z < LocationUtils.Floor2.getBlockZ(); z++){
    					
    					pl.sendBlockChange(new Location(LocationUtils.Floor1.getWorld(), x, LocationUtils.Floor1.getBlockY(), z), Material.SNOW_BLOCK, (byte) 1);
    					
    					
    				}
    			}
    			for(int x = LocationUtils.Floor2.getBlockX(); x < LocationUtils.Floor1.getBlockX(); x++){
    				for(int z = LocationUtils.Floor2.getBlockZ(); z < LocationUtils.Floor1.getBlockZ(); z++){
    					
    					pl.sendBlockChange(new Location(LocationUtils.Floor1.getWorld(), x, LocationUtils.Floor1.getBlockY(), z), Material.SNOW_BLOCK, (byte) 1);
    					
    					
    				}
    			}
    			
    		}
    		
    	}
    	
        a.removeScoreboard();
        
        a.getPlayers().remove(p.getName());
        
        if(a.getPlayers().size() > 0){
        for(Player pl : Bukkit.getOnlinePlayers()){
        	
        	if(a.getPlayers().contains(pl.getName())){
        		
        		if(!(pl.getName().equalsIgnoreCase(p.getName()))){
        			if(a.isInGame() == true){
        			if(a.getPlayers().size() == 1){
        				if(pl != p){
        				Winner(pl);
        				}
        				removePlayer(pl);
        			}
        			}
        		}
        		
        	}
        }
        	
        }
        
        a.removeFromTeam(p);
        
        for(Player pl : Bukkit.getOnlinePlayers()){
        	
        	p.showPlayer(pl);
        	pl.showPlayer(p);
        	
        }
       
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);

        p.getInventory().setContents(inv.get(p.getName()));
        p.getInventory().setArmorContents(armor.get(p.getName()));
        p.setGameMode(GameMode.CREATIVE);

        inv.remove(p.getName());
        armor.remove(p.getName());
        p.teleport(locs.get(p.getName()));
        locs.remove(p.getName());
        
        p.setFireTicks(0);
              
        p.sendMessage(ChatColor.GREEN + "You are now in the lobby!");
    }

    @SuppressWarnings("deprecation")
	public Arena createArena(){
        int num = arenaSize + 1;
        arenaSize++;

        Arena a = new Arena(num);
        arenas.add(a);

        List<Integer> list = Main.instance.getConfig().getIntegerList("Arenas.Arenas");
        list.add(num);
        Main.instance.getConfig().set("Arenas.Arenas", list);
        Main.instance.saveConfig();
        
        for(Player p : Bukkit.getOnlinePlayers()){
        	p.sendMessage(ChatColor.GREEN + "Arena" + num + " created!");
        }
        
        a.setRound(1);
        
        return a;
    }

    public Arena reloadArena() {
        int num = arenaSize + 1;
        arenaSize++;
 
        Arena a = new Arena(num);
        arenas.add(a);
        
        a.setRound(1);
 
        return a;
    }
    
    public void removeArena(int i) {
        Arena a = getArena(i);
        if(a == null) {
            return;
        }
        arenas.remove(a);

        Main.instance.getConfig().set("Arenas." + i, null);
        List<Integer> list = Main.instance.getConfig().getIntegerList("Arenas.Arenas");
        list.remove(i);
        Main.instance.getConfig().set("Arenas.Arenas", list);
        Main.instance.saveConfig();    
    }

    public boolean isInGame(Player p){
        for(Arena a : arenas){
            if(a.getPlayers().contains(p.getName()))
                return true;
        }
        return false;
    }

    public void loadGames(){
        arenaSize = 0;      

        if(Main.instance.getConfig().getIntegerList("Arenas.Arenas").isEmpty()){
            return;
        }
                
        for(int i : Main.instance.getConfig().getIntegerList("Arenas.Arenas")){
            Arena a = reloadArena();
            a.id = i;
        }
    }
    
    public void Winner(Player p){
    	
    	p.sendMessage(ChatColor.GOLD + "You have won!");
    	
    }

}
